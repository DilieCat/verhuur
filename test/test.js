var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../index.js');

chai.should();

chai.use(chaiHttp);

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var mail = makeid(6)


describe('Users', () => {

    it('Ceate new user and post', function (done) {
        chai.request(server)
            .post('/auth/register')
            .set('Content-Type', 'application/json')
            .send({
                "email": mail + "@hotmail.com",
                "password": "test123",
                "firstname": "Pilivio",
                "surname": "Cat",
                "street": "Pearlbuck-erf 39",
                "postalCode": "3313 CB",
                "city": "Dordrecht",
                "telNumber": "0635086767",
                "birthday": "1970-09-04"
            })
            .end(function (err, res, body) {
                res.should.have.status(200);
                done()
            })
    });

    it('Ceate new user and post with wrong postal code', function (done) {
        chai.request(server)
            .post('/auth/register')
            .set('Content-Type', 'application/json')
            .send({
                "email": "Dil3443_90@hotmail.com",
                "password": "test123",
                "firstname": "Pilivio",
                "surname": "Cat",
                "street": "Pearlbuck-erf 20",
                "postalCode": "323423434315 CB",
                "city": "Dordrecht",
                "telNumber": "0635086767",
                "birthday": "1970-09-04"
            })
            .end(function (err, res, body) {
                res.should.have.status(500);
                done()
            })
    });

    it('Login with user credentials, should pass', function (done) {
        chai.request(server)
            .post('/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": mail + "@hotmail.com",
                "password": "test123",
            })
            .end(function (err, res, body) {
                res.should.have.status(200);
                done()
            })
    });


    it('Get all appartments', function (done) {
        chai.request(server)
            .get('/api/appartments')
            .set('Content-Type', 'application/json')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMDkxLCJleHAiOjE1NTg4MDUwMTUsImlhdCI6MTU1Nzk0MTAxNX0.K_KYNrt3d29f1Smgm98obJ2p_gfBI2HZ5b5Gxqsf3d0')
            .end(function (err, res, body) {
                res.should.have.status(200);
                done()
            })
    });

    it('Get all reservationns', function (done) {
        chai.request(server)
            .get('/api/appartments/12358/reservations/')
            .set('Content-Type', 'application/json')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMDkxLCJleHAiOjE1NTg4MDUwMTUsImlhdCI6MTU1Nzk0MTAxNX0.K_KYNrt3d29f1Smgm98obJ2p_gfBI2HZ5b5Gxqsf3d0')
            .end(function (err, res, body) {
                res.should.have.status(200);
                done()
            })
    });

    it('Get 404 on uknown endpoint in auth router', function (done) {
        chai.request(server)
            .get('/auth/oisadguaewoioigsiougsipgeuiiugi')
            .set('Content-Type', 'application/json')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMDkxLCJleHAiOjE1NTg4MDUwMTUsImlhdCI6MTU1Nzk0MTAxNX0.K_KYNrt3d29f1Smgm98obJ2p_gfBI2HZ5b5Gxqsf3d0')
            .end(function (err, res, body) {
                res.should.have.status(404);
                done()
            })
    });

    it('Get 404 on uknown endpoint in api router', function (done) {
        chai.request(server)
            .get('/api/oisadguaewoioigsiougsipgeuiiugi')
            .set('Content-Type', 'application/json')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMDkxLCJleHAiOjE1NTg4MDUwMTUsImlhdCI6MTU1Nzk0MTAxNX0.K_KYNrt3d29f1Smgm98obJ2p_gfBI2HZ5b5Gxqsf3d0')
            .end(function (err, res, body) {
                res.should.have.status(404);
                done()
            })
    });


});