const express = require("express");
const assert = require("assert");
const router = express.Router();
const Appartment = require("../model/appartment");
const Reservation = require("../model/reservation");
const jwt = require("../helpers/jwt");
const db = require("../data/mysql-connector")
var jwtDecode = require('jwt-decode')
var token = "";
var userId = "";

//Check if user token is valid.
router.all("*", function (req, res, next) {
  assert(
    typeof req.headers["x-access-token"] == "string",
    "token is not a string!"
  );


  token = req.header("X-Access-Token") || "";
  userId = "" + jwtDecode(token).sub

  jwt.decodeToken(token, (err, payload) => {
    if (err) {
      console.log("Error handler: " + err.message);
      next(err);
    } else {
      next();
    }
  });
});



//Get all appartments
router.get("/appartments/", (req, res, next) => {
  const query = {
    sql: `
      SELECT ApartmentId, Description, apartment.StreetAddress, apartment.PostalCode, apartment.City, apartment.UserId, FirstName, Lastname, user.StreetAddress as userStreet, user.PostalCode as userPostalCode, user.City as userCity, user.DataOfBirth, user.PhoneNumber FROM apartment JOIN user ON apartment.UserId = user.UserId
      `,
    values: [],
    timeout: 2000
  }
  try {
    db.query(query, (err, rows, fields) => {
      if (err) {
        next(err)
      } else {
        let appartments = []
        rows.forEach(element => {

          appartments.push({
            "description": element.Description,
            "street": element.StreetAddress,
            "postalCode": element.PostalCode,
            "city": element.City,
            "user": {
              "firstname": element.FirstName,
              "lastname": element.LastName,
              "street": element.userStreet,
              "postalCode": element.userPostalCode,
              "city": element.userCity,
              "birthday": element.DataOfBirth,
              "telNumber": element.PhoneNumber,
              "email": element.EmailAddress
            }
          })
        })
        res.status(200).json(appartments)
      }
    })
  } catch (err) {
    console.log(err)
    next(err)
  }
})

//Create appartment
router.post("/appartments/", function (req, res, next) {
  try {
    const appartment = new Appartment(req.body.description, req.body.street, req.body.postalCode, req.body.city);
    // Construct query object
    const query = {
      sql: "INSERT INTO `apartment`(Description, StreetAddress, PostalCode, City, UserId) VALUES (?,?,?,?,?)",
      values: [appartment.description, appartment.street, appartment.postalCode, appartment.city, userId],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(err);
        res.status(500)
      } else {
        res.status(200).json(rows);
      }
    });
  } catch (ex) {
    next(ex);
  }
})

//Get appartment from given ID
router.get("/appartments/:id", (req, res, next) => {
  const id = req.params.id
  const query = {
    sql: `
      SELECT * FROM apartment WHERE ApartmentId = (?)
      `,
    values: [id],
    timeout: 2000
  }
  try {
    db.query(query, (err, result, fields) => {
      if (err) {
        next(err)
      } else {
        res.status(200).json(result)
      }
    })
  } catch (err) {
    console.log(err)
    next(err)
  }
});

//Edit an appartment
router.put("/appartments/:id", (req, res, next) => {
  const id = req.params.id

  //Get user data first, check if user data from the database matches the current user
  const queryUserCheck = {
    sql: `
      SELECT UserId FROM apartment WHERE ApartmentId = (?)
      `,
    values: [id],
    timeout: 2000
  }

  try {
    db.query(queryUserCheck, (err, row, fields) => {
      if (row[0].UserId != userId) {
        res.status(401).json({
          "Message": "You are not allowed to edit this appartment."
        })
      } else {

        //Start updating the appartment
        let appartment = new Appartment(req.body.description, req.body.street, req.body.postalCode, req.body.city)

        const query = {
          sql: `
      UPDATE apartment SET Description = (?), StreetAddress = (?), PostalCode = (?), City = (?) WHERE ApartmentId = (?)
      `,
          values: [appartment.description, appartment.street, appartment.postalCode, appartment.city, id],
          timeout: 2000
        }
        try {
          db.query(query, (err, result, fields) => {
            if (err) {
              next(err)
            } else if (result.affectedRows == 0) {
              res.status(400).json({
                "Message": "Appartment does not exist"
              })
            } else {
              res.status(200).json(appartment)
            }
          })
        } catch (err) {
          console.log(err)
          next(err)
        }
      }
    })
  } catch (err) {
    console.log(err)
    next(err)
  }
});

//Delete an appartment
router.delete("/appartments/:id", (req, res, next) => {
  const id = req.params.id

  //Get user data first, check if user data from the database matches the current user
  const queryUserCheck = {
    sql: `
      SELECT UserId FROM apartment WHERE ApartmentId = (?)
      `,
    values: [id],
    timeout: 2000
  }

  try {
    db.query(queryUserCheck, (err, row, fields) => {
      if (row[0].UserId != userId) {
        res.status(401).json({
          "Message": "You are not allowed to delete this appartment."
        })
      } else {
        //Start deleting appartment
        const query = {
          sql: `
      DELETE FROM apartment WHERE ApartmentId = (?)
      `,
          values: [id],
          timeout: 2000
        }
        try {
          db.query(query, (err, result, fields) => {
            if (err) {
              next(err)
            } else if (result.affectedRows == 0) {
              res.status(400).json({
                "Message": "Appartment does not exist"
              })
            } else {
              res.status(200).json({
                "Message": "Appartment is deleted"
              })
            }
          })
        } catch (err) {
          console.log(err)
          next(err)
        }
      }
    })
  } catch (err) {
    console.log(err)
    next(err)
  }
});

//Create a reservation for a appartment
router.post("/appartments/:id/reservations", function (req, res, next) {
  const id = req.params.id
  var appartmentUserId = 5

  const queryUserId = {
    sql: "SELECT * FROM apartment WHERE ApartmentId = (?)",
    values: [id],
    timeout: 2000
  };

  // Perform query
  db.query(queryUserId, (err, rows, fields) => {
    if (err) {
      console.log(err);
      next(err);
      res.status(500)
    } else {
      appartmentUserId = rows[0].UserId
      console.log(rows[0].UserId)


      console.log(appartmentUserId)

      try {

        const reservation = new Reservation(req.body.startDate, req.body.endDate);


        // Construct query object
        const query = {
          sql: "INSERT INTO `reservation` (StartDate, EndDate, Status, UserId, ApartmentId) VALUES (?,?,?,?,?)",
          values: [reservation.startDate, reservation.endDate, "INITIAL", rows[0].UserId, id],
          timeout: 2000
        };

        // Perform query
        db.query(query, (err, rows, fields) => {
          if (err) {
            console.log(err);
            next(err);
            res.status(500)
          } else {
            res.status(200).json(rows);
          }
        });
      } catch (ex) {
        next(ex);
      }
    }
  });
})

//Request all reservations for a specific appartment
router.get("/appartments/:id/reservations", function (req, res, next) {
  const id = req.params.id
  try {
    // Construct query object
    const query = {
      sql: "SELECT * FROM reservation WHERE ApartmentId = (?)",
      values: [id],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(err);
        res.status(500)
      } else {
        res.status(200).json(rows);
      }
    });
  } catch (ex) {
    next(ex);
  }
})

//Requesting specific reservation
router.get("/appartments/:id/reservations/:reserveId", function (req, res, next) {
  const id = req.params.id
  const reserveId = req.params.reserveId
  try {
    // Construct query object
    const query = {
      sql: "SELECT * FROM reservation WHERE ApartmentId = (?) AND ReservationId = (?)",
      values: [id, reserveId],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(err);
        res.status(500)
      } else {
        res.status(200).json(rows);
      }
    });
  } catch (ex) {
    next(ex);
  }
})

//Change reservation status
router.put("/appartments/:id/reservations/:reserveId", function (req, res, next) {
  const id = req.params.id
  const reserveId = req.params.reserveId

  //Get user data first, check if user data from the database matches the current user
  const queryUserCheck = {
    sql: `
      SELECT UserId FROM apartment WHERE ApartmentId = (?)
      `,
    values: [id],
    timeout: 2000
  }

  try {
    db.query(queryUserCheck, (err, row, fields) => {
      if (row[0].UserId != userId) {
        res.status(401).json({
          "Message": "You are not allowed to edit this appartment."
        })
      } else {
        try {
          // Construct query object
          const query = {
            sql: "UPDATE reservation SET Status = (?) WHERE ApartmentId = (?) AND ReservationId = (?)",
            values: [req.body.status, id, reserveId],
            timeout: 2000
          };
          // Perform query
          db.query(query, (err, row, fields) => {
            if (err) {
              console.log(err);
              next(err);
              res.status(500)
            } else {
              res.status(200).json(row);
            }

          })
        } catch (ex) {
          next(ex);
        }
      }
    })
  } catch (err) {
    console.log(err)
    next(err)
  }
})

router.delete("/appartments/:id/reservations/:userReserveId", (req, res, next) => {
  const id = req.params.id
  const userReserveId = req.params.userReserveId

  const query = {
    sql: `
      DELETE FROM reservation WHERE UserId = (?) AND ApartmentId = (?)
      `,
    values: [userReserveId, id],
    timeout: 2000
  }
  try {
    db.query(query, (err, result, fields) => {
      if (err) {
        next(err)
      } else {
        res.status(200).json(result)
      }
    })
  } catch (err) {
    console.log(err)
    next(err)
  }
});

// Fall back, display some info
router.all("*", function (req, res) {
  res.status(404);
  res.json({
    description: "This page is not found!!"
  });
});

module.exports = router;