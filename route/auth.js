const express = require("express");
const assert = require("assert");
const router = express.Router();
const User = require("../model/user");
const db = require("../data/mysql-connector.js");
const bcrypt = require("bcryptjs");
const jwt = require("../helpers/jwt");

const saltRounds = 10;

router.post("/register", function (req, res, next) {
  try {
    const hash = bcrypt.hashSync(req.body.password, saltRounds);
    const user = new User(req.body.email, hash, req.body.firstname, req.body.surname, req.body.street, req.body.postalCode, req.body.city, req.body.telNumber, req.body.birthday);
    // Construct query object
    const query = {
      sql: "INSERT INTO `user`(FirstName, LastName, StreetAddress, PostalCode, City, DataOfBirth, PhoneNumber, EmailAddress, Password) VALUES (?,?,?,?,?,?,?,?,?)",
      values: [user.firstname, user.surname, user.street, user.postalCode, user.city, user.birthday, user.telNumber, user.email, user.password],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(err);
      } else {
        res.status(200).json(rows);
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//
// Login with username / password
//
router.post("/login", function (req, res, next) {
  try {
    // Validate with assert is string etc ..
    assert(typeof req.body.password === "string", "Password is not a string!");
    assert(typeof req.body.email === "string", "email is not a string!");

    // Construct query object
    const query = {
      sql: "SELECT * FROM `user` WHERE `EmailAddress`= (?)",
      values: [req.body.email],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        next(err);
      } else {
        console.log(rows)
        if (
          rows.length === 1 &&
          bcrypt.compareSync(req.body.password, rows[0].Password)
        ) {
          token = jwt.encodeToken(rows[0].UserId);
          res.status(200).json({
            token: token
          });
        } else {
          next(new Error("Invalid login"));
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

// Fall back, display some info
router.all("*", function (req, res) {
  res.status(404);
  res.json({
    description: "This page is not found!!"
  });
});

module.exports = router;