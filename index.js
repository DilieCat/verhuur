const express = require("express");
const app = express();
const auth = require("./route/auth")
const api = require("./route/api")
const bodyParser = require("body-parser");


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.send({ hello: "world" });
});

app.use("/auth", auth)
app.use("/api", api);

const PORT = process.env.PORT || 8080;
app.listen(PORT, function() {
  console.log(`App listening onono porto ${PORT}`);
});

module.exports = app