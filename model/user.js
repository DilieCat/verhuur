class User {
  constructor(email, password, firstname, surname, street, postalCode, city, telNumber, birthday) {
    (this.email = this.validateEmail(email)),
    (this.password = password),
    (this.firstname = firstname),
    (this.surname = surname),
    (this.street = street),
    (this.postalCode = this.validatePostalCode(postalCode)),
    (this.city = city),
    (this.telNumber = this.validateTelNumber(telNumber)),
    (this.birthday = birthday);
  }

  //Validate email
  validateEmail(email) {
    let regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (regex.test(email)) {
      return email;
    } else {
      throw new Error("Invalid Email");
    }
  }

  //Validate postalcode
  validatePostalCode(postalCode) {
    var regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
    if (regex.test(postalCode)) {
      return postalCode;
    } else {
      throw new Error("Invalid Postal Code");
    }
  }

  validateTelNumber(telNumber) {
    if (this.vastOrMobiel(telNumber)) {
      return telNumber;
    } else {
      throw new Error("Invalid Phone Number");
    }
  }

  //Check of number is vast or mobiel, then return the value.
  vastOrMobiel(phone) {
    var vast_nummer = /^(((0)[1-9]{2}[0-9][-]?[1-9][0-9]{5})|((\\+31|0|0031)[1-9][0-9][-]?[1-9][0-9]{6}))$/;
    var mobiel_nummer = /^(((\\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/i;
    return (vast_nummer.test(phone) || mobiel_nummer.test(phone));
  }
}


module.exports = User;

/*Een gebruiker heeft een voornaam, achternaam, straat met huisnummer, postcode, woonplaats,
telefoonnummer, emailadres, en een wachtwoord. Een postcode en een telefoonnummer moeten
voldoen aan het juiste formaat, anders zijn ze niet geldig en kan een gebruiker zich niet registreren.*/