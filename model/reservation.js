class Reservation {
    constructor(startDate, endDate) {
        (this.startDate = this.validateStartDate(startDate)),
        (this.endDate = this.validateEndDate(endDate, startDate))
    }

    //startDate
    validateStartDate(startDate) {
        var today = new Date();
        var startingDate = new Date(startDate);
        var temp = today.toLocaleDateString("nl-NL")
        var date = temp.replace('/', "-").replace('/', "-")

        if (startingDate > today) {
            return date;
        } else {
            throw Error("Starting date needs to be after today!")
        }
    }

    validateEndDate(endDate, startDate) {
        var endingDate = new Date(endDate);
        var startingDate = new Date(startDate);
        var temp = endingDate.toLocaleDateString("nl-NL")
        var date = temp.replace('/', "-").replace('/', "-")

        if (endingDate > startingDate) {
            return date;
        } else {
            throw Error("Ending date needs to be after the starting date!")
        }
    }

}

module.exports = Reservation;