class Appartment {
  constructor(description, street, postalCode, city) {
    (this.description = description),
    (this.street = street),
    (this.postalCode = postalCode),
    (this.city = city)
  }

  //Validate postalcode
  validatePostalCode(postalCode) {
    var regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
    if (regex.test(postalCode)) {
      return postalCode;
    } else {
      throw new Error("Invalid Postal Code");
    }
  }
}

module.exports = Appartment;